import { ClientDashboardDal } from "../../basic/beDal/clientDashboard.dal";

export class SecondService {
  constructor(private dal: ClientDashboardDal) {
  }

  public async getName(): Promise<string> {
    return this.dal.getName();
  }
}
