import { ClientDashboardModel } from "../contract";
import { ClientDashboardBEService } from "../beServices/clientDashboard.service";
import { SecondService } from "../beServices/second.service";

export class ClientDashboardApi {
  constructor(private service: ClientDashboardBEService, private service2: SecondService) { }

  public async getClientDashboard(): Promise<ClientDashboardModel> {
    const name = await this.service.getName();
    const name2 = await this.service2.getName();
    return { name, name2 };
  }
}
