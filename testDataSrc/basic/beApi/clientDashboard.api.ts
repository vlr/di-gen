import { ClientDashboardModel } from "../contract";
import { ClientDashboardBEService } from "../beServices/clientDashboard.service";

export class ClientDashboardApi {
  constructor(private service: ClientDashboardBEService) { }

  public async getClientDashboard(): Promise<ClientDashboardModel> {
    const name = await this.service.getName();
    return { name };
  }
}
