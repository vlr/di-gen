import { ClientDashboardDal } from "../beDal/clientDashboard.dal";

export class ClientDashboardBEService {
  constructor(private dal: ClientDashboardDal) { }
  public async getName(): Promise<string> {
    return await this.dal.getName();
  }
}
