import { IBEContext } from "../beContext";

export class ClientDashboardDal {
  constructor(private context: IBEContext) { }

  public async getName(): Promise<string> {
    return this.context.name;
  }
}
