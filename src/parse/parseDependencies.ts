import { Dependency, dependency } from "./types";

export function parseDependencies(ctorLine: string): Dependency[] {
  return ctorLine.split(",")
    .map(line => line.trim())
    .map(line => parseDependency(line))
    .filter(line => line != null);
}

function parseDependency(depLine: string): Dependency {
  const split = depLine.split(":").map(item => item.trim());
  if (split.length !== 2) { return null; }
  return dependency(removeAccessModifier(split[0]), split[1]);
}

function removeAccessModifier(name: string): string {
  return name
    .replace(/^private\s+/, "")
    .replace(/^public\s+/, "")
    .replace(/^protected\s+/, "");
}
