import { parseImports, Import } from "@vlr/imports-gen";
import { ContentFile } from "../types";
import { parseDependencies } from "./parseDependencies";
import { ParsedClass, parsedClass, Dependency } from "./types";
import { contains } from "@vlr/array-tools";

export function parseFiles(files: ContentFile[]): ParsedClass[] {
  return files.map(parseFile).filter(x => x != null);
}

export const classRegex = /export\s+class\s+([^\s\{]+)\s*{(.*)}/s;
export const constructorRegex = /constructor\(([^\)]*)\)\s*{[^\}]*}/s;

// might want to design better parser in later releases
export function parseFile(file: ContentFile): ParsedClass {
  const classMatch = file.contents.match(classRegex);
  if (classMatch == null) {
    return null;
  }
  const name = classMatch[1];
  const constructorMatch = classMatch[2].match(constructorRegex);
  if (constructorMatch == null) {
    return parsedClass(file, name, [], []);
  }
  const dependencies = parseDependencies(constructorMatch[1]);
  const imports = parseImports(file.contents);
  const filtered = filterImports(imports, dependencies);
  return parsedClass(file, name, filtered, dependencies);
}

function filterImports(imports: Import[], dependencies: Dependency[]): Import[] {
  const types = dependencies.map(d => d.type);
  return imports.filter(imp => contains(types, imp.alias));
}
