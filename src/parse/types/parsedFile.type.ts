import { ContentFile } from "@vlr/gulp-transform";
import { Import } from "./import.type";

export interface ParsedClass {
  file: ContentFile; // source file
  className: string; // name оf parsed class
  imports: Import[]; // imports in the source file
  dependencies: Dependency[];
}

export interface Dependency {
  varName: string; // name of injected variable
  type: string; // type of the dependency
}
