import { createDiModels } from "./createDiModel/createDiModel";
import { parseFiles } from "./parse/parseFiles";
import { ContentFile, DiOptions } from "./types";
import { produce } from "./produce/produce";

const defaultOptions: DiOptions = {
  quotes: "\"",
  linefeed: "\n"
};

export function generateDi(files: ContentFile[], options: DiOptions): ContentFile[] {
  options = { ...defaultOptions, ...options };
  const parsed = parseFiles(files);
  const diModels = createDiModels(parsed);
  return produce(diModels, options);
  return [];
}
