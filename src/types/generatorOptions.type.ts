import { GeneratorOptions } from "@vlr/razor/export";

export interface DiOptions extends GeneratorOptions {
  tsconfig?: any;
}
