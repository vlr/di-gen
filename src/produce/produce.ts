import { DiOptions, ContentFile } from "../types";
import { di } from "./generators/di";
import { changeFileType } from "@vlr/gulp-transform";
import { createProdModel } from "./createProdModel/createProdModel";
import { DiModel } from "../createDiModel/types";

export function produce(models: DiModel[], options: DiOptions): ContentFile[] {
  return models.map(model => produceFile(model, options));
}

function produceFile(model: DiModel, options: DiOptions): ContentFile {
  const prod = createProdModel(model);
  return {
    ...model.file,
    name: changeFileType(model.file.name, ".di"),
    contents: di.generate(prod, options)
  };
}
