export function createDiName(name: string): string {
  const first = name[0];
  return first.toLowerCase() + name.slice(1);
}
