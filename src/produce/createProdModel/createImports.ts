import { DiModel, DiDependency, CtorDependency } from "../../createDiModel/types";
import { Import } from "../../parse/types";
import { createImportPath } from "./createImportPath";
import { changeFileType } from "@vlr/gulp-transform";
import { diFileType } from "../../diFileType";
import { createDiName } from "./createDiName";

export function createImports(model: DiModel): Import[] {
  const importedExternals = model.diDependencies.map(dep => createExternalImport(model.file.folder, dep));
  const importedDiForCtor = model.ctorDependencies.filter(x => !x.isExternal).map(ctor => createCtorImport(model.file.folder, ctor));
  const ownImport = createOwnImport(model);
  return [ownImport, ...importedExternals, ...importedDiForCtor];
}

function createOwnImport(model: DiModel): Import {
  const importPath = "./" + model.file.name;
  return {
    importPath,
    type: model.id.typeName,
    alias: model.id.typeName
  };
}

function createExternalImport(path: string, dep: DiDependency): Import {
  var importPath = createImportPath(path, dep.id.folder, dep.id.file);
  return {
    importPath,
    type: dep.id.typeName,
    alias: dep.id.typeName
  };
}

function createCtorImport(path: string, ctor: CtorDependency): Import {
  var importPath = createImportPath(path, ctor.id.folder, changeFileType(ctor.id.file, diFileType));
  var type = createDiName(ctor.id.typeName);
  return {
    importPath,
    type,
    alias: type
  };
}
