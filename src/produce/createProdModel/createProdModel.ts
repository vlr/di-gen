import { DiModel, DiDependency, CtorDependency } from "../../createDiModel/types";
import { DiProdModel, diProdModel, ProdDependency, prodDependency } from "../types";
import { createDiName } from "./createDiName";
import { createImports } from "./createImports";

export function createProdModel(model: DiModel): DiProdModel {
  const diName = createDiName(model.id.typeName);
  const imports = createImports(model);
  const diParameters = concatenateDependencies(model.diDependencies);
  const ctorDependencies = model.ctorDependencies.map((ctor, i) => ctor.isExternal ? ctor : { ...ctor, varName: "d" + i });
  const dependencies = ctorDependencies.filter(ctor => !ctor.isExternal).map(createProdDependency);
  const ctorString = ctorDependencies.map(ctor => ctor.varName).join(", ");
  return diProdModel(model.id.typeName, diName, imports, diParameters, dependencies, ctorString);
}

function createProdDependency(dep: CtorDependency): ProdDependency {
  const diName = createDiName(dep.id.typeName);
  return prodDependency(dep.varName, diName, concatenateDependencyParameters(dep.diDependencies));
}

function concatenateDependencyParameters(deps: DiDependency[]): string {
  return deps.map(dep => dep.varName).join(", ");
}

function concatenateDependencies(dependencies: DiDependency[]): string {
  return dependencies.map(dep => `${dep.varName}: ${dep.id.typeName}`).join(", ");
}
