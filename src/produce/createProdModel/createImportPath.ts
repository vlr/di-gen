export function createImportPath(srcPath: string, destPath: string, file: string): string {
  let pathSplit = destPath.split("/");
  let srcSplit = srcPath.split("/");
  while (pathSplit.length && srcSplit.length && pathSplit[0] === srcSplit[0]) {
    pathSplit = pathSplit.slice(1);
    srcSplit = srcSplit.slice(1);
  }

  const resultSplit = [...srcSplit.map(s => ".."), ...pathSplit];
  const result = resultSplit.join("/");
  if (!result.length) {
    return ".";
  }

  return `${result}/${file}`;
}
