import { Import } from "../../parse/types";
import { ProdDependency } from "./prodDependency.type";

export interface DiProdModel {
  className: string; // name of the source class
  diName: string; // name of the di function
  imports: Import[]; // prod di imports
  diParametersString: string; // concatenated line of external dependencies
  ctorDependencies: ProdDependency[];
  ctorParametersString: string;
}


