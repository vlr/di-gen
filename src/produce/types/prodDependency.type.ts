export interface ProdDependency {
  varName: string; // name of variable
  diName: string; // name of di constructor
  dependencies: string; // concatenated names of passed external dependencies
}
