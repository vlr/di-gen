import { last } from "@vlr/array-tools";

export function getAbsoluteFolderPath(srcFolder: string, importPath: string): string {
  if (importPath[0] !== ".") {
    return importPath;
  }

  const srcSplit = srcFolder.split("/");
  const importSplit = importPathSplit(importPath);
  return calculateDiff(srcSplit, importSplit);
}

function importPathSplit(src: string): string[] {
  src = src.replace(/\\/g, "/");
  const lastIndex = src.lastIndexOf("/");
  src = src.slice(0, lastIndex);
  return src.split("/").filter(part => part.length);
}

function calculateDiff(path: string[], imp: string[]): string {
  imp.forEach(line => {
    if (line === "..") {
      path = subtractPath(path);
    } else if (line !== ".") {
      path = [...path, line];
    }
  });
  return path.join("/");
}

function subtractPath(path: string[]): string[] {
  const lastLine = last(path);
  if (lastLine === ".") {
    const lastRemoved = path.slice(0, path.length - 1);
    return [...lastRemoved, ".."];
  } else if (lastLine === "..") {
    return [...path, ".."];
  } else {
    return path.slice(0, path.length - 1);
  }
}
