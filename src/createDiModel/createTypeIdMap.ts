import { toMap } from "@vlr/map-tools";
import { Import, ParsedClass } from "../parse/types";
import { ContentFile } from "../types";
import { TypeIdMap, typeId, TypeId } from "./types";
import { getAbsoluteFolderPath } from "./getAbsoluteFolderPath";

export function createTypeIdMap(parsed: ParsedClass): TypeIdMap {
  return toMap(parsed.imports, i => i.alias, i => createTypeId(parsed.file, i));
}

function createTypeId(file: ContentFile, imp: Import): TypeId {
  const folder = getAbsoluteFolderPath(file.folder, imp.importPath);
  const fileName = getImportedFile(imp.importPath);
  return typeId(folder, fileName, imp.type);
}

export function getImportedFile(importPath: string): string {
  const lastSlash = importPath.lastIndexOf("/");
  return importPath.slice(lastSlash + 1);
}
