import { TypeId } from "./typeId.type";

export interface DiDependency {
  id: TypeId;
  varName: string;
}
