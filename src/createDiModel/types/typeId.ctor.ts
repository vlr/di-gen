// warning! This code was generated by @vlr/type-constructor-gen
// manual changes to this file will be overwritten next time the code is regenerated.
import { TypeId } from "./typeId.type";

// methods for TypeId
export function typeId(folder: string, file: string, typeName: string): TypeId {
  return {
    folder,
    file,
    typeName,
  };
}
