import { TypeId } from "./typeId.type";
import { CtorDependency } from "./ctorDependency.type";
import { ContentFile } from "../../types";
import { DiDependency } from "./diDependency.type";

export interface DiModel {
  id: TypeId;
  file: ContentFile;
  ctorDependencies: CtorDependency[];
  diDependencies: DiDependency[];
}
