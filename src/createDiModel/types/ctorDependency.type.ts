import { TypeId } from "./typeId.type";
import { DiDependency } from "./diDependency.type";

export interface CtorDependency {
  id: TypeId;
  varName: string;

  // these flags are populated during the second run
  isExternal: boolean;
  diDependencies: DiDependency[];
}
