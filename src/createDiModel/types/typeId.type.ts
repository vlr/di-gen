export interface TypeId {
  folder: string;
  file: string;
  typeName: string;
}

export type TypeIdMap = Map<string, TypeId>;
