import { flatMap, unique } from "@vlr/array-tools";
import { KeyMap } from "@vlr/map-tools";
import { CtorDependency, DiModel, TypeId } from "./types";

export type ModelMap = KeyMap<TypeId, DiModel>;

export function updateModelDiDependencies(id: TypeId, map: ModelMap): DiModel {
  const model = map.get(id);
  if (model == null || model.diDependencies != null) { return model; }
  const updated = getUpdatedModel(model, map);
  map.add(updated);
  return updated;
}

function getUpdatedModel(model: DiModel, map: ModelMap): DiModel {
  const ctorDependencies = model.ctorDependencies.map(dep => updateCtorDependency(dep, map));
  const diDependencies = flatMap(ctorDependencies, ctor => ctor.isExternal ? [ctor] : ctor.diDependencies);
  const uniqueDiDependencies = unique(diDependencies, d => d.id);
  return {
    ...model,
    ctorDependencies,
    diDependencies: uniqueDiDependencies
  };
}

function updateCtorDependency(dep: CtorDependency, map: KeyMap<TypeId, DiModel>): CtorDependency {
  const model = updateModelDiDependencies(dep.id, map);
  if (model == null) { return { ...dep, isExternal: true }; }
  return {
    ...dep,
    isExternal: false,
    diDependencies: model.diDependencies
  };
}
