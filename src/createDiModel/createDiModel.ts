import { ParsedClass } from "../parse/types";
import { createEmptyModel } from "./createEmptyModel";
import { DiModel } from "./types";
import { updateModelDiDependencies } from "./populateDiDependencies";
import { mapBy } from "@vlr/map-tools";

export function createDiModels(parsed: ParsedClass[]): DiModel[] {
  const models = parsed.map(createEmptyModel);
  const populated = populateDiDependencies(models);
  return populated;
}

export function populateDiDependencies(models: DiModel[]): DiModel[] {
  const map = mapBy(models, m => m.id);
  return models.map(model => updateModelDiDependencies(model.id, map));
}
