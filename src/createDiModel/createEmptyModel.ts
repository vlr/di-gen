import { Dependency, ParsedClass } from "../parse/types";
import { createTypeIdMap } from "./createTypeIdMap";
import { ctorDependency, CtorDependency, DiModel, diModel, TypeId, typeId, TypeIdMap } from "./types";

export function createEmptyModel(parsed: ParsedClass): DiModel {
  const id = createTypeId(parsed);
  const typeIdMap = createTypeIdMap(parsed);
  const dependencies = parsed.dependencies.map(d => createCtorDependency(d, typeIdMap));
  return diModel(id, parsed.file, dependencies, null);
}

function createTypeId(parsed: ParsedClass): TypeId {
  return typeId(parsed.file.folder, parsed.file.name, parsed.className);
}

function createCtorDependency(dependency: Dependency, idMap: TypeIdMap): CtorDependency {
  const id = idMap.get(dependency.type);
  return ctorDependency(id, dependency.varName, null, null);
}
