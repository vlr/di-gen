import { settings } from "./settings";
import { constants } from "./constants";

export function testFolderFiles(folder: string): string {
  return `${testFolderFull(folder)}/${constants.allTs}`;
}

export function testFolderFull(folder: string): string {
  return `${settings.testFolder}/${folder}`;
}
