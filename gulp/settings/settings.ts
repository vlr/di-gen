export const settings = {
  src: "src",
  testSrc: "testSrc",
  build: "./build",
  buildEs5: "./build/es5",
  buildEs6: "./build/es6",
  testFolder: "./testFolder"
};
