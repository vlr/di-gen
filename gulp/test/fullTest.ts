import { series } from "gulp";
import { runTestCoverage, runTestsEs5, runTestsEs6 } from "./parts";
import { fullBuild } from "../build/fullBuild";
import { prepareGenTest } from "../prepareGenTest/prepareGenTest";

export const fullTest = series(
  fullBuild,
  prepareGenTest,
  runTestsEs5, runTestsEs6,
  runTestCoverage,
);
