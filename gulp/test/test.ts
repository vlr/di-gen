import { series } from "gulp";
import { build } from "../build/build";
import { runTests, runTestCoverage } from "./parts";
import { prepareGenTest } from "../prepareGenTest/prepareGenTest";

export const test = series(build, prepareGenTest, runTests);

export const cover = series(build, prepareGenTest, runTestCoverage);
