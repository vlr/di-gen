import * as gulp from "gulp";
import { transform, ContentFile } from "@vlr/gulp-transform";
import { allTargetFiles } from "../../settings";
import { settings } from "../../settings";

const regex = /((?:const|var) [^\s]* = require\(["|']\@vlr\/[^"\/]*)((?:\/[^"]*)?["|']\);)/g;

function replace(file: ContentFile, target: string): ContentFile {
  return {
    ...file,
    contents: file.contents.replace(regex, (all, grp1, grp2) => grp1 + "/" + target + grp2)
  };
}

function addImportTarget(folder: string, target: string): NodeJS.ReadWriteStream {
  return gulp.src(allTargetFiles(folder + "temp"))
    .pipe(transform(file => replace(file, target)))
    .pipe(gulp.dest(`./${folder}`));
}

export function addEs5ImportTarget(): NodeJS.ReadWriteStream {
  return addImportTarget(settings.buildEs5, "es5");
}

export function addEs6ImportTarget(): NodeJS.ReadWriteStream {
  return addImportTarget(settings.buildEs6, "es6");
}
