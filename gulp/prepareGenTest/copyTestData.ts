import * as gulp from "gulp";
import { settings } from "../settings";

export function copyTestData(): NodeJS.ReadWriteStream {
  return gulp.src("./testDataSrc/**/*.*")
    .pipe(gulp.dest(settings.testFolder));
}
