import { series } from "gulp";
import { runGenerator } from "./runGenerator";
import { clearTestFolder } from "./clearTestFolder";
import { copyTestData } from "./copyTestData";
import { compileTestFolder } from "./compileTestFolder";

export const prepareGenTest = series(
  clearTestFolder,
  copyTestData,
  runGenerator,
  compileTestFolder
);
