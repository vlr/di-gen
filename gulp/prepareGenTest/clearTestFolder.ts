import { removeDir } from "../tools/removeDir";
import { settings } from "../settings";

export function clearTestFolder(): Promise<void> {
  return removeDir(settings.testFolder);
}
