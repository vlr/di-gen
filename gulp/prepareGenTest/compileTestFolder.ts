import { spawn } from "@vlr/spawn";
import { parallel } from "gulp";

export const compileTestFolder = parallel(compileTestFolderLatest, compileTestFolderEs5, compileTestFolderEs6);

export function compileTestFolderLatest(): Promise<void> {
  return spawn("compileTestFolder", "tsc", "--build", "tsconfig.test.json");
}

export function compileTestFolderEs5(): Promise<void> {
  return spawn("compileTestFolderEs5", "tsc", "--build", "tsconfig.testEs5.json");
}

export function compileTestFolderEs6(): Promise<void> {
  return spawn("compileTestFolderEs6", "tsc", "--build", "tsconfig.testEs6.json");
}
