import { series, src, dest } from "gulp";
import { testFolderFiles, testFolderFull } from "../settings/testFolderFiles";
import { transformRange } from "@vlr/gulp-transform";

function execGenerator(folder: string): NodeJS.ReadWriteStream {
  // tslint:disable-next-line: no-require-imports
  const { generateDi } = require("../../build/src");
  return src(testFolderFiles(folder))
    .pipe(transformRange(files => generateDi(files)))
    .pipe(dest(testFolderFull(folder)));
}

const folders = ["basic", "twoBranches"];

export const runGenerator = series(...folders.map(f => () => execGenerator(f)));

