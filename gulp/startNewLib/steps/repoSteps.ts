import * as fse from "fs-extra";
import { spawnIt } from "@vlr/spawn";
import { projectPath } from "./projectName";

export function replaceRemoteInPackage(): Promise<void> {
  return replaceInFile("package.json", replaceLibName);
}

export function replaceRemoteInGitlabCi(): Promise<void> {
  return replaceInFile(".gitlab-ci.yml", replaceLibName);
}

function replaceLibName(content: string): string {
  return content.replace(/vlr\/tslib-seed-level2/g, projectPath());
}

async function replaceInFile(file: string, replacer: (f: string) => string): Promise<void> {
  const content = await fse.readFile(file, "utf8");
  const result = replacer(content);
  await fse.writeFile(file, result);
}

export async function installNodeModules(): Promise<void> {
  await spawnIt("npm ci", "npm", ["ci"], {});
}
