import { ClientDashboardApi } from "../../testDataSrc/basic/beApi/clientDashboard.api";
import { expect } from "chai";

describe("generated code, basic", function (): void {
  it("should create a dependency chain to return name", async function (): Promise<void> {
    // arrange
    var { clientDashboardApi } = require("../../testFolder/basic/beApi/clientDashboard.di");
    var context = { name: "testName" };
    var target: ClientDashboardApi = clientDashboardApi(context);

    // act
    const result = await target.getClientDashboard();

    // assert
    expect(result.name).equals(context.name);
  });
});
