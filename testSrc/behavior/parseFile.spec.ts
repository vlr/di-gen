import { expect } from "chai";
import { parseFile } from "../../src/parse/parseFiles";
import { ContentFile } from "../../src/types";
import { ParsedClass, parsedClass, Import, Dependency, dependency } from "../../src/parse/types";

describe("parseFile", function (): void {
  it("should parse a class", function (): void {
    // arrange
    const contents = `
    import { ClientDashboardModel } from "../contract";
    import { ClientDashboardBEService } from "../beServices";

    export class ClientDashboardApi {
      constructor(private service: ClientDashboardBEService) {}

      public async getClientDashboard(): Promise<ClientDashboardModel> {
        const accountInfo = await this.service.getAccountInfo();
        return clientDashboardModel(accountInfo);
      }
    }
`;
    const file: ContentFile = {
      contents,
      name: null, extension: null, cwd: null, base: null, folder: null
    };

    const imports: Import[] = [
      { type: "ClientDashboardBEService", alias: "ClientDashboardBEService", importPath: "../beServices" }
    ];
    const dependencies: Dependency[] = [
      dependency("service", "ClientDashboardBEService")
    ];

    const expected: ParsedClass = parsedClass(file, "ClientDashboardApi", imports, dependencies);

    // act
    const result = parseFile(file);

    // assert
    expect(result).deep.equals(expected);
  });

  it("should return null if no class", function (): void {
    // arrange
    const contents = `
    export function someFunction(): void {
      return null;
    }
`;
    const file: ContentFile = {
      contents,
      name: null, extension: null, cwd: null, base: null, folder: null
    };

    // act
    const result = parseFile(file);

    // assert
    expect(result == null).equals(true);
  });

  it("should return class with no dependencies", function (): void {
    // arrange
    const contents = `
    export class ClientDashboardApi {
      public async getClientDashboard(): Promise<ClientDashboardModel> {
        const accountInfo = await this.service.getAccountInfo();
        return clientDashboardModel(accountInfo);
      }
    }
`;
    const file: ContentFile = {
      contents,
      name: null, extension: null, cwd: null, base: null, folder: null
    };

    // act
    const result = parseFile(file);
    const expected: ParsedClass = parsedClass(file, "ClientDashboardApi", [], []);

    // assert
    expect(result).deep.equals(expected);
  });
});
