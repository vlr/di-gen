import { ClientDashboardApi } from "../../testDataSrc/twoBranches/beApi/clientDashboard.api";
import { expect } from "chai";

describe("generated code, twoBranches", function (): void {
  it("should create a dependency chain to return name", async function (): Promise<void> {
    // arrange
    var { clientDashboardApi } = require("../../testFolder/twoBranches/beApi/clientDashboard.di");
    var context = { name: "testName" };
    var target: ClientDashboardApi = clientDashboardApi(context);

    // act
    const result = await target.getClientDashboard();

    // assert
    expect(result.name).equals(context.name);
    expect(result.name2).equals(context.name);
  });
});
