import { expect } from "chai";
import { getAbsoluteFolderPath } from "../src/createDiModel/getAbsoluteFolderPath";

describe("getAbsoluteFolderPath", function (): void {
  it("should return same directory for next file", async function (): Promise<void> {
    // arrange
    const filePath = "./someDir/dir2";
    const importPath = "./file";
    const expected = "./someDir/dir2";

    // act
    const result = getAbsoluteFolderPath(filePath, importPath);

    // assert
    expect(result).equals(expected);
  });

  it("should return same directory from root", async function (): Promise<void> {
    // arrange
    const filePath = ".";
    const importPath = "./file";
    const expected = ".";

    // act
    const result = getAbsoluteFolderPath(filePath, importPath);

    // assert
    expect(result).equals(expected);
  });

  it("should return path of subdirectory file", async function (): Promise<void> {
    // arrange
    const filePath = "./parent";
    const importPath = "./child/file";
    const expected = "./parent/child";

    // act
    const result = getAbsoluteFolderPath(filePath, importPath);

    // assert
    expect(result).equals(expected);
  });

  it("should return path of parent directory", async function (): Promise<void> {
    // arrange
    const filePath = "./parent/child";
    const importPath = "../file";
    const expected = "./parent";

    // act
    const result = getAbsoluteFolderPath(filePath, importPath);

    // assert
    expect(result).equals(expected);
  });

  it("should return path of neighbour directory or parent file", async function (): Promise<void> {
    // arrange
    const filePath = "./parent/child";
    const importPath = "../neighbour/file";
    const expected = "./parent/neighbour";

    // act
    const result = getAbsoluteFolderPath(filePath, importPath);

    // assert
    expect(result).equals(expected);
  });

  it("should return path from outside root", async function (): Promise<void> {
    // arrange
    const filePath = "./parent";
    const importPath = "../../otherPath/file";
    const expected = "../otherPath";

    // act
    const result = getAbsoluteFolderPath(filePath, importPath);

    // assert
    expect(result).equals(expected);
  });

  it("should not alter non-aliased path", async function (): Promise<void> {
    // arrange
    const filePath = "./someDir/dir2/file";
    const importPath = "some-lib";
    const expected = "some-lib";

    // act
    const result = getAbsoluteFolderPath(filePath, importPath);

    // assert
    expect(result).equals(expected);
  });

});
